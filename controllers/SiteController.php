<?php

namespace app\controllers;

use app\models\NasdaqForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\httpclient\Client;

class SiteController extends Controller
{
    const ORDER             = 'asc';
    const DATABASE_CODE     = 'WIKI';
    const REQUEST_METHOD    = 'GET';

    const DATE_COLUMN_NAME      = 'Date';
    const OPEN_COLUMN_NAME      = 'Open';
    const CLOSE_COLUMN_NAME     = 'Close';
    const VOLUME_COLUMN_NAME    = 'Volume';

    const FROM_DATE_MORE_TO_DATE_MDG    = 'Неверные даты. Начальная дата не может быть больше конечной';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new NasdaqForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionNasdaqByCompany() {
        $request = Yii::$app->request;
        $indexColumnDate    = null;
        $indexColumnOpen    = null;
        $indexColumnClose   = null;
        $indexColumnVolume  = null;
        $dateValueArray     = [];
        $openValueArray     = [];
        $closeValueArray    = [];

        if ($request->isPost) {
            $qp = $request->post('NasdaqForm');
            $url = 'https://www.quandl.com/api/v3/datasets/'
                . self::DATABASE_CODE
                . '/'
                . $qp['companySymbol']
                . '.json';
            $client = new Client();
            if (strtotime($qp['startDate']) > strtotime($qp['endDate'])) {
                Yii::$app->session->setFlash('warning', self::FROM_DATE_MORE_TO_DATE_MDG);
                return $this->goHome();
            }

            $response = $client->createRequest()
                ->setMethod(self::REQUEST_METHOD)
                ->setUrl($url)
                ->setData([
                    'order'         => self::ORDER,
                    'start_date'    => $qp['startDate'],
                    'end_date'      => $qp['endDate']
                ])->send();

            if (!empty($response->data['dataset']['data'])) {
                $data = $response->data['dataset'];

                foreach ($data['column_names'] as $columnKey => $columnName) {
                    switch ($columnName) {
                        case self::DATE_COLUMN_NAME :
                            $indexColumnDate = $columnKey;
                            break;
                        case self::OPEN_COLUMN_NAME :
                            $indexColumnOpen = $columnKey;
                            break;
                        case self::CLOSE_COLUMN_NAME :
                            $indexColumnClose = $columnKey;
                            break;
                        case self::VOLUME_COLUMN_NAME :
                            $indexColumnVolume = $columnKey;
                            break;
                    }
                }
                foreach ($data['data'] as $key => $item) {
                    array_push($dateValueArray, $item[$indexColumnDate]);
                    array_push($openValueArray, $item[$indexColumnOpen]);
                    array_push($closeValueArray, $item[$indexColumnClose]);
                }

                return $this->render('result', [
                    'data'          => $data,
                    'indexColumn'   => [
                        'date' => $indexColumnDate,
                        'open' => $indexColumnOpen,
                        'close' => $indexColumnClose,
                        'volume' => $indexColumnVolume,
                    ],
                    'dateValueArray'    => $dateValueArray,
                    'openValueArray'    => $openValueArray,
                    'closeValueArray'   => $closeValueArray,
                ]);
            }

            throw new NotFoundHttpException('По запросу с данными параметрами ничего не найдено!');
        }

        throw new NotFoundHttpException('Для вывода информации необходимо заполнить на главной странице!');
    }
}
