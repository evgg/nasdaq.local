<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class NasdaqForm extends Model
{
    public $companySymbol;
    public $startDate;
    public $endDate;
    public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['companySymbol', 'startDate', 'endDate', 'email',], 'required'],
            [['startDate', 'endDate'], 'date', 'format' => 'yyyy-mm-dd'],
            // email has to be a valid email address
            [['email'], 'email'],
            [['email'], 'string', 'max' => 50]
        ];
    }

    public function attributeLabels()
    {
        return [
            'companySymbol' => 'Аббревиатура компании',
            'startDate'     => 'Начальная дата',
            'endDate'       => 'Конечная дата',
            'email'         => 'Email'
        ];
    }
}
