<?php

/* @var $this yii\web\View */
/* @var $model \app\models\NasdaqForm */

use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <p class="lead">Для отображения данных необходимо заполнить форму</p>
    </div>

    <div class="body-content">

        <section class="mh_bottom">

            <?php $form = ActiveForm::begin([
                'action' => Url::to(['nasdaq-by-company']),
                'id' => 'nasdaq-form',
                'options' => ['class' => 'nasdaq-form'],
            ]); ?>

            <div class="row">

                <div class="col-md-6">
                    <?= $form->field($model, 'companySymbol')->textInput() ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'email') ?>
                </div>

            </div>

            <div class="form-group form-group_datepicker">

                <p style="margin-left: 10px;"><strong>Выберете интервал</strong></p>
                <?= DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'startDate',
                    'attribute2' => 'endDate',
                    'separator' => '<span class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span>',
                    'options' => [
                        'placeholder' => 'Начальная дата',
                        'autocomplete' => 'off',
                    ],
                    'options2' => [
                        'placeholder' => 'Конечная дата',
                        'autocomplete' => 'off',
                    ],
                    'type' => DatePicker::TYPE_RANGE,
                    'form' => $form,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ]
                ]); ?>

            </div>

            <div class="form-group">
                    <?= Html::submitButton(
                        '<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>&nbsp;Загрузить',
                        [
                            'class' => 'btn btn-primary',
                            'name' => 'contact-button'
                        ]) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </section>

    </div>
</div>
