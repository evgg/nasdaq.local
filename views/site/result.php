<?php

/* @var $data array */
/* @var $indexColumn array */
/* @var $dateValueArray array */
/* @var $openValueArray array */
/* @var $closeValueArray array */

use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = 'Nasdaq';
$this->params['breadcrumbs'][] = $this->title;

?>

<section class="table-result">
    <div class="table-result__controll text-right">
        <?= Html::button('<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> График открывающих цен', [
            'class'         => 'btn btn-success',
            'data-toggle'   => 'modal',
            'data-target'   => '#js-open-column-modal',
        ]) ?>
        <?= Html::button('<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> График закрывающих цен', [
            'class'         => 'btn btn-danger',
            'data-toggle'   => 'modal',
            'data-target'   => '#js-close-column-modal',
        ]) ?>

    </div>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <?php foreach ($data['column_names'] as $columnKey => $columnName) : ?>
                        <th><?= $columnName ?></th>
                        <?php if ($columnKey == $indexColumn['volume']) break; ?>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php $count = 1; ?>
                <?php foreach ($data['data'] as $row) : ?>
                    <tr>
                        <th scope="row"><?= $count++ ?></th>

                        <?php foreach ($row as $key => $value) : ?>
                            <td><?= $value ?></td>
                            <?php  if ($key == $indexColumn['volume']) break; ?>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</section>

<?php
    Modal::begin([
        'header'    => '<h3>Chart open column</h3>',
        'size' => 'modal-lg',
        'closeButton' => [
            'label' => '<span aria-hidden="true">&times;</span>',
        ],
        'footer' => Html::button('Закрыть', [
            'class' => 'btn btn-info',
            'data-dismiss' => "modal"
        ]),
        'options'   => [
            'id' => 'js-open-column-modal',
        ]
    ]);
?>

    <div class="text-center">
        <canvas id="js-open-chart" width="780" height="400"></canvas>
    </div>

<?php Modal::end(); ?>

<?php
    Modal::begin([
        'header'    => '<h3>Chart close column</h3>',
        'size' => 'modal-lg',
        'closeButton' => [
            'label' => '<span aria-hidden="true">&times;</span>',
        ],
        'footer' => Html::button('Закрыть', [
            'class' => 'btn btn-info',
            'data-dismiss' => "modal"
        ]),
        'options'   => [
            'id' => 'js-close-column-modal',
        ]
    ]);
?>

    <div class="text-center">
        <canvas id="js-close-chart" width="780" height="400"></canvas>
    </div>

<?php Modal::end(); ?>

<?php
$dateValueArray = json_encode($dateValueArray);
$openValueArray = json_encode($openValueArray);
$closeValueArray = json_encode($closeValueArray);

$script = <<< JS
    (function($) {
        var dateValueArray    = JSON.parse('$dateValueArray');
        var openValueArray    = JSON.parse('$openValueArray');
        var closeValueArray   = JSON.parse('$closeValueArray');
        var ctxClose  = document.getElementById("js-close-chart");
        var ctxOpen   = document.getElementById("js-open-chart");

        var ctxOpen = new Chart(ctxOpen, {
            type: 'line',
            data: {
            labels: dateValueArray,
            datasets: [
                { 
                    data: openValueArray,
                    label: "Open index",
                    borderColor: "#5cb85c",
                    fill: true
                }
            ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Nasdaq open price'
                }
            }
        });
        var myLineChart = new Chart(ctxClose, {
            type: 'line',
            data: {
            labels: dateValueArray,
            datasets: [
                { 
                    data: closeValueArray,
                    label: "Close index",
                    borderColor: "#c9302c",
                    fill: true
                }
            ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Nasdaq close price'
                }
            }
        });
    })(jQuery)
JS;
$this->registerJs($script);
?>
